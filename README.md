# README #

Accompanying source code for blog entry at http://tech.asimio.net/2016/04/05/Microservices-using-Spring-Boot-Jersey-Swagger-and-Docker.html

### How do I get set up? ###

* Building and executing it from command line
```
# mvn clean package
# java -jar target/springboot-jersey-swagger-docker.jar
```

Open http://localhost:8000 in a browser

### How do I get set up using Docker? ###

```
# sudo docker pull asimio/springboot-jersey-swagger-docker
# sudo docker run -idt -p 8701:8701 -e appPort=8701 asimio/springboot-jersey-swagger-docker:latest
```

Open http://localhost:8701 in a browser

### Who do I talk to? ###

* ootero at asimio dot net
* https://www.linkedin.com/in/ootero